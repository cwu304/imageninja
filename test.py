from scipy import misc
import numpy
from numpy import linalg
import matplotlib.pyplot as plot

im=misc.imread('lena.jpg')

rec=numpy.zeros(im.shape,dtype=numpy.uint8) #zero arrays uint8 0-255
print rec
for rgb in range(3):
    for i in range(im.shape[0]):
        for j in range(im.shape[1]):
            rec[i][j][rgb]=im[i][j][rgb]
print '----------rec----------- \n'
print rec
print '----------im------------ \n'
print im

#rec.astype(numpy.uint8) #conert to uint8 0-255
fig=plot.figure(1)
plot.imshow(rec) #, cmap=plot.cm.gray
plot.show()

result=linalg.svd(rec)
rec2 = result[1]
fig2=plot.figure(2)
plot.imshow(rec2) #, cmap=plot.cm.gray
plot.show()
