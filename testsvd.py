from scipy import misc
import numpy
from numpy import linalg
import matplotlib.pyplot as plot
from numpy import linalg
from math6643 import SVD_Solver
import copy
import math


a = [[22.,10., 2.,  3., 7.],
      [14., 7.,10.,  0., 8.],
      [-1.,13.,-1.,-11., 3.],
      [-3.,-2.,13., -2., 4.],
      [ 9., 8., 1., -2., 4.],
      [ 9., 1.,-7.,  5.,-1.],
      [ 2.,-6., 6.,  5., 1.],
      [ 4., 5., 0., -2., 2.]]

sbuilt = SVD_Solver(a)
sbuilt.run_builtin()
print '-------built in :---------'
print sbuilt.S

my = SVD_Solver(a)
my.run_implementation_1()
print '-------our result :---------'
print my.S
s2 = numpy.zeros((8,5))
for i in range(5):
    s2[i,i] = my.S[0,i]
print my.U
s3 = numpy.dot(my.U,s2)
print 's3 \n', s3
s4= numpy.dot(s3,my.V)
print 's4 \n',s4
#print my.U
#print my.V



