from __future__ import division

'''
this file reads from training/ all the image files and then write the training result to train.txt

'''
import glob
import os
from scipy import misc
import numpy
from numpy import linalg

#bw is the compress ratio, return result=[U,S,V,favg, filelist, xilist]
def train6643(bw):
    os.chdir('training/')
    N=0
    avg=[]
    filelist=glob.glob('*')
    for file in filelist:
        image=misc.imread(file)
        if N==0:
            avg=image
        else:
            avg=avg+image
        N=N+1
        
    avg=avg/N
    #print avg
    
    m=0
    for i in range(1,avg.shape[0]-1,bw):
        m=m+1

    n=0
    for j in range(1,avg.shape[1]-1,bw):
        n=n+1
    #print 'compressed image is {0} by {1}'.format(m,n)

    #now compress the image
    M=m*n
    A=numpy.zeros((M,N))
    favg=numpy.zeros((M,1))
    A=numpy.asmatrix(A)
    favg=numpy.asmatrix(favg)
    im_num=0
    for file in filelist:
        image=misc.imread(file)
        count=0
        for i in range(1,avg.shape[0]-1, bw):
            for j in range(1,avg.shape[1]-1,bw):
                tot=9
                A[count,im_num]=image[i,j]/tot+image[i-1,j]/tot+image[i+1,j]/tot\
                    +image[i,j-1]/tot+image[i,j+1]/tot+image[i-1,j-1]/tot\
                    +image[i-1,j+1]/tot+image[i+1,j-1]/tot+image[i+1,j+1]/tot
                count=count+1
        
        favg=favg+A[:, im_num]
        im_num=im_num+1
        
    favg=favg/N
    #print favg

    for nn in range(N):
        A[:,nn]=A[:,nn]-favg
    os.chdir('../')

    result=linalg.svd(A)
    xilist=[]
    for nn in range(N):
        xi=result[0].T*A[:,nn]
        xilist.append(xi)
        #print 'epsilon for {0} is {1}'.format(filelist[nn], linalg.norm(xi))
    result=result+(favg,)
    result=result+(filelist,)
    result=result+(xilist,)
    return result


#train6643(3)
