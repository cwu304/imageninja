import os
import matplotlib.pyplot as plot
import numpy

stat=open('./results/analysis.txt')

accuracy=[]
instr=[]
ep_correct=[]
ep_wrong=[]
info=open('./results/README')
for line in info:
    words=line.split('\n')
    instr.append(words[0][2:])


while True:
    line=stat.readline()
    if not line:
        break
    #not useful
    line=stat.readline()
    line=stat.readline()
    #the accuracy
    line=stat.readline()
    words=line.split(',')
    if len(words)<2:
        break
    accuracy.append(float(words[2]))
    #not useful
    line=stat.readline()
    #the epsilon
    line=stat.readline()
    #print line
    words=line.split(',')
    ep_correct.append(float(words[0]))
    ep_wrong.append(float(words[1]))
    #\n
    line=stat.readline()
'''
print accuracy
print instr
print ep_correct
print ep_wrong
'''

fig=1
figure=plot.figure(fig)
fig+=1
draw1=[accuracy[0],accuracy[2],accuracy[4]]
plot.plot(range(1,4),draw1,'r')
plot.plot(range(1,4),draw1,'ro')

draw2=[accuracy[1],accuracy[3],accuracy[5]]
plot.plot(range(1,4),draw2,'green')
plot.plot(range(1,4),draw2,'go')

plot.xlabel('Number of images trained per person')
plot.ylabel('Accuracy')
plot.title('Fig.1 Recognition Accuracy vs Training Images per Person')
plot.xticks([1,2,3])
plot.text(1.1,0.88,'RED LINE: All files included',color='red')
plot.text(1.1,0.86,'GREEN LINE: Left/Right-Lighted excluded',color='green')

figure=plot.figure(fig)
fig+=1
draw3=ep_correct[0:5:2]
plot.plot(range(1,4),draw3,'ro')
draw4=ep_wrong[0:5:2]
plot.plot(range(1,4),draw4,'r^')

draw5=ep_correct[1:6:2]
plot.plot(range(1,4),draw5,'go')
draw6=ep_wrong[1:6:2]
plot.plot(range(1,4),draw6,'g^')

plot.xticks(range(5))
plot.yticks(range(2000,8000,1000))
plot.ylabel('Epsilon')
plot.xlabel('Number of images trained per person')
plot.text(2.2,5000,'Triangular: incorrect recognition')
plot.text(2.2,5250,'Circle: correct recognition')
plot.text(2.2,5500,'Red: All files included', color='red')
plot.text(2.2,5750,'Green: Left/right-lighted excluded',color='green')
plot.title('Fig.2 Epislon: distance to closest face')

figure=plot.figure(fig)
fig+=1

choose=[0,2,6,7]
draw7=[accuracy[i] for i in choose]
plot.plot(range(1,5),draw7,'r^')
plot.yticks(numpy.arange(0.3,0.9,0.1))
plot.xticks(numpy.arange(0.5,5,0.5))
frame=plot.gca()
frame.get_xaxis().set_ticks([])
plot.ylabel('accuracy')
plot.text(1,0.59,'trained on normal')
plot.text(2,0.72,'trained on normal and wink')
plot.text(3.05,0.35,'trained on left/right lighted')
plot.text(3.,0.64,'trained on normal and left-lighted')
plot.title('Fig.3 accurarcy and special images')
plot.show()
