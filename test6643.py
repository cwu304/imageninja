from __future__ import division
import glob
import numpy
from numpy import linalg
from scipy import misc
import sys
from train6643 import train6643




def test6643(train,raw_test,bw):
    #compress the test image first
    m=0
    for i in range(1,raw_test.shape[0]-1,bw):
        m=m+1
    n=0
    for j in range(1,raw_test.shape[1]-1,bw):
        n=n+1
    
    #print 'train is {0} by {1}'.format(m,n)
    #print train[0].shape[0]
    
    #test image and train image must has matching size
    if(m*n!=train[0].shape[0]):
        return -1
    #flatten the test image
    M=m*n
    test=numpy.zeros((M,1))
    test=numpy.asmatrix(test)
    count=0
    for i in range(1,raw_test.shape[0]-1,bw):
        for j in range(1,raw_test.shape[1]-1,bw):
            test[count,0]=raw_test[i,j]
            count=count+1

    favg=train[3]
    x=train[0].T*(test-favg)
    filelist=train[4]
    fp=train[0]*x
    xilist=train[5]
    
    prediction=10000
    for i in range(len(filelist)):
        epsilon=linalg.norm(xilist[i]-x)
        #print 'distance to {0} is {1}'.format(filelist[i], epsilon)
        if i==0:
            min_epsilon=epsilon
            preditcion=filelist[i]
        elif min_epsilon>epsilon:
            min_epsilon=epsilon
            prediction=filelist[i]
    
    return (prediction,min_epsilon)


'''
filelist=glob.glob('yalefaces/*')
train=train6643(3)
for file in filelist:
    #print file
    image=misc.imread(file)
    test=test6643(train,image,3)
    print '{0},{1},{2}'.format(file,test[0],test[1])
'''

'''
filename=sys.argv[1]
#print 'filename is {}'.format(filename)
image=misc.imread(filename)
train=train6643(3)
test=test6643(train,image,3)
print '{0},{1},{2}'.format(filename,test[0],test[1])
'''
