from __future__ import division
from numpy import linalg
from scipy import misc
import glob
import numpy
import os
import copy
import math

class SVD_Solver:
    def __init__(self,im):
        self.A=im
        self.U=None
        self.S=None
        self.V=None

    def run_builtin(self):
        result=linalg.svd(self.A)
        self.U=result[0]
        self.S=result[1]
        self.V=result[2]
        #print 'U \n', self.U
        #print 'S \n', self.S
        #print 'V \n', self.V

    
    def run_implementation_1(self):
        
		'''Compute the singular value decomposition of array.'''

		# Golub and Reinsch state that eps should not be smaller than the
		# machine precision, ie the smallest number
		# for which 1+e>1.  tol should be beta/e where beta is the smallest
		# positive number representable in the computer.
		eps = 1.e-15  # assumes double precision
		tol = 1.e-64/eps
		assert 1.0+eps > 1.0 # if this fails, make eps bigger
		assert tol > 0.0     # if this fails, make tol bigger
		itmax = 50
		u = 1.0*copy.deepcopy(self.A)
		m = len(self.A)
		n = len(self.A[0])
		#if __debug__: print 'a is ',m,' by ',n

		if m < n:
			if __debug__: print 'Error: m is less than n'
			raise ValueError,'SVD Error: m is less than n.'

		e = [0.0]*n  # allocate arrays
		q = [0.0]*n
		v = []
		for k in range(n): v.append([0.0]*n)
	 
		# Householder's reduction to bidiagonal form

		g = 0.0
		x = 0.0

		for i in range(n):
			e[i] = g
			s = 0.0
			l = i+1
			for j in range(i,m): s += (u[j][i]*u[j][i])
			if s <= tol:
				g = 0.0
			else:
				f = u[i][i]
				if f < 0.0:
					g = math.sqrt(s)
				else:
					g = -math.sqrt(s)
				h = f*g-s
				u[i][i] = f-g
				for j in range(l,n):
					s = 0.0
					for k in range(i,m): s += u[k][i]*u[k][j]
					f = s/h
					for k in range(i,m): u[k][j] = u[k][j] + f*u[k][i]
			q[i] = g
			s = 0.0
			for j in range(l,n): s = s + u[i][j]*u[i][j]
			if s <= tol:
				g = 0.0
			else:
				f = u[i][i+1]
				if f < 0.0:
					g = math.sqrt(s)
				else:
					g = -math.sqrt(s)
				h = f*g - s
				u[i][i+1] = f-g
				for j in range(l,n): e[j] = u[i][j]/h
				for j in range(l,m):
					s=0.0
					for k in range(l,n): s = s+(u[j][k]*u[i][k])
					for k in range(l,n): u[j][k] = u[j][k]+(s*e[k])
			y = abs(q[i])+abs(e[i])
			if y>x: x=y
		# accumulation of right hand transformations
		for i in range(n-1,-1,-1):
			if g != 0.0:
				h = g*u[i][i+1]
				for j in range(l,n): v[j][i] = u[i][j]/h
				for j in range(l,n):
					s=0.0
					for k in range(l,n): s += (u[i][k]*v[k][j])
					for k in range(l,n): v[k][j] += (s*v[k][i])
			for j in range(l,n):
				v[i][j] = 0.0
				v[j][i] = 0.0
			v[i][i] = 1.0
			g = e[i]
			l = i
		#accumulation of left hand transformations
		for i in range(n-1,-1,-1):
			l = i+1
			g = q[i]
			for j in range(l,n): u[i][j] = 0.0
			if g != 0.0:
				h = u[i][i]*g
				for j in range(l,n):
					s=0.0
					for k in range(l,m): s += (u[k][i]*u[k][j])
					f = s/h
					for k in range(i,m): u[k][j] += (f*u[k][i])
				for j in range(i,m): u[j][i] = u[j][i]/g
			else:
				for j in range(i,m): u[j][i] = 0.0
			u[i][i] += 1.0
		#diagonalization of the bidiagonal form #Givens rotation
		eps = eps*x
		for k in range(n-1,-1,-1):
			for iteration in range(itmax):
				# test f splitting
				for l in range(k,-1,-1):
					goto_test_f_convergence = False
					if abs(e[l]) <= eps:
						# goto test f convergence
						goto_test_f_convergence = True
						break  # break out of l loop
					if abs(q[l-1]) <= eps:
						# goto cancellation
						break  # break out of l loop
				if not goto_test_f_convergence:
					#cancellation of e[l] if l>0
					c = 0.0
					s = 1.0
					l1 = l-1
					for i in range(l,k+1):
						f = s*e[i]
						e[i] = c*e[i]
						if abs(f) <= eps:
							#goto test f convergence
							break
						g = q[i]
						h = self.pythag(f,g)
						q[i] = h
						c = g/h
						s = -f/h
						for j in range(m):
							y = u[j][l1]
							z = u[j][i]
							u[j][l1] = y*c+z*s
							u[j][i] = -y*s+z*c
				# test f convergence
				z = q[k]
				if l == k:
					# convergence
					if z<0.0:
						#q[k] is made non-negative
						q[k] = -z
						for j in range(n):
							v[j][k] = -v[j][k]
					break  # break out of iteration loop and move on to next k value
				if iteration >= itmax-1:
					if __debug__: print 'Error: no convergence.'
					# should this move on the the next k or exit with error??
					#raise ValueError,'SVD Error: No convergence.'  # exit the program with error
					break  # break out of iteration loop and move on to next k
				# shift from bottom 2x2 minor
				x = q[l]
				y = q[k-1]
				g = e[k-1]
				h = e[k]
				f = ((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y)
				g = self.pythag(f,1.0)
				if f < 0:
					f = ((x-z)*(x+z)+h*(y/(f-g)-h))/x
				else:
					f = ((x-z)*(x+z)+h*(y/(f+g)-h))/x
				# next QR transformation
				c = 1.0
				s = 1.0
				for i in range(l+1,k+1):
					g = e[i]
					y = q[i]
					h = s*g
					g = c*g
					z = self.pythag(f,h)
					e[i-1] = z
					c = f/z
					s = h/z
					f = x*c+g*s
					g = -x*s+g*c
					h = y*s
					y = y*c
					for j in range(n):
						x = v[j][i-1]
						z = v[j][i]
						v[j][i-1] = x*c+z*s
						v[j][i] = -x*s+z*c
					z = self.pythag(f,h)
					q[i-1] = z
					c = f/z
					s = h/z
					f = c*g+s*y
					x = -s*g+c*y
					for j in range(m):
						y = u[j][i-1]
						z = u[j][i]
						u[j][i-1] = y*c+z*s
						u[j][i] = -y*s+z*c
				e[l] = 0.0
				e[k] = f
				q[k] = x
				# goto test f splitting
			
				
		vt = self.transpose(v)
		#return (u,q,vt)
		#return (u,q,v)
		#print 'u'
		#print 'q'
		#print 'v'
		# convert to matrix
		um = numpy.matrix(u)
		qm = numpy.matrix(q)
		vtm = numpy.matrix(vt)
		print '-----------before rearrange-------'
		print 'u is \n',   um
		#print 'q is \n',  qm
		#print 'vt is \n',  vtm
		#rearrange
		#print qm0[0]
		#print n
		for i in range(n-1):
			index = -1
			temp = qm[0,i]
			#print i 
			for j in range(i,n):
					if qm[0,j] > temp:
						temp = qm[0,j]
						index = j
			if index >=0 :
				#print 'swap'			
				qm[0,i],qm[0,index] = qm[0,index],qm[0,i]
				#swap columns of um
				temp1 = copy.deepcopy(um[:,i])
				um[:,i] = um[:,index]
				um[:,index] = temp1
				#swap rows of vtm
				temp2 = copy.deepcopy(vtm[i])
				vtm[i] = vtm[index]
				vtm[index] = temp2
				#print 'q swap is \n', qm
		um0 = numpy.zeros((m,m))
		um0[:,0:n] = um
		print '-----------after rearrange-------'
		print 'u is \n', um0
		print 'q is \n', qm
		print 'vt is \n',  vtm
		#convert to array
		self.U = um0
		qm2 = numpy.zeros(m)
		for i in range(m):
			qm2[i] = qm[0,i]
		self.S = qm2
		self.V = numpy.array(vtm)
    		
    def swap(self,a,b):
		temp = copy.deepcopy(a)
		a = b
		b = temp
    		
    def pythag(self,a,b):
		absa = abs(a)
		absb = abs(b)
		if absa > absb: return absa*math.sqrt(1.0+(absb/absa)**2)
		else:
			if absb == 0.0: return 0.0
			else: return absb*math.sqrt(1.0+(absa/absb)**2)	
			
    def transpose(self,a):
		'''Compute the transpose of a matrix.'''
		m = len(a)
		n = len(a[0])
		at = []
		for i in range(n): at.append([0.0]*m)
		for i in range(m):
			for j in range(n):
				at[j][i]=a[i][j]
		return at	
	
    def find_approximate(self,rank):
        approx=numpy.zeros(self.A.shape)
		#print approx.shape
        #print 'u shape \n',self.U.shape,type(self.U)
        #print 'v shape \n',self.V.shape,type(self.V)
        #print 's shape \n',self.S.shape,type(self.S)
        for k in range(rank):
            row=self.U[:,k]
            column=self.V[k,:]
            #print 'row ', row.shape
            #print 'column',column.shape
            approx=approx+numpy.outer(row,column)*self.S[k]
        return approx

