from __future__ import division
import sys
import re

score=0
minus=0
total=0

avg_correct=0
avg_wrong=0
light_effect=0
with open(sys.argv[1]) as file:
    for line in file:
        words=re.split('\W+',line)

        total=total+1
        info=re.findall('\d+',line)
        pt=re.findall('\d+.',line)
        if info[0] == info[1]:#correct guess
            score=score + 1
            avg_correct+=int(float(pt[2]))
        else:#incorrect guess
            minus=minus+1
            avg_wrong+=int(float(pt[2]))
            if words[2]==words[4]:
                light_effect+=1





avg_correct=avg_correct/score
avg_wrong=avg_wrong/minus

print 'analyzing {}'.format(sys.argv[1])
print '?0 out of ?1 = ?2 are correct'
print '{0},{1},{2}'.format(score,total,score/total)
#print '?0 out of ?1 = ?2 are incorrect as they have the same style'
#print '{0},{1},{2}'.format(light_effect,minus,light_effect/minus)

print 'average correct/incorrect epsilon'
print '{0},{1}'.format(avg_correct,avg_wrong)

print '\n'
