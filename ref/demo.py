import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons, Button

t = np.arange(0.0, 2.0, 0.01)
s0 = np.sin(2*np.pi*t)
s1 = np.sin(4*np.pi*t)
s2 = np.sin(6*np.pi*t)
s = [t-1, 2*t-1, 3*t-1]

fig, ax = plt.subplots()
l0, = ax.plot(t, s0, visible=False, lw=2)
l1, = ax.plot(t, s1, visible=False, lw=2)
l2, = ax.plot(t, s2, visible=False, lw=2)
l, = ax.plot(t,s[0])

plt.subplots_adjust(left=0.2)


rax1 = plt.axes([0.05, 0.4, 0.1, 0.15])
check = CheckButtons(rax1, ('2 Hz', '4 Hz', '6 Hz'), (False, False, False))

def check_callback(label):
    if label == '2 Hz': l0.set_visible(not l0.get_visible())
    elif label == '4 Hz': l1.set_visible(not l1.get_visible())
    elif label == '6 Hz': l2.set_visible(not l2.get_visible())
    plt.draw()
    
check.on_clicked(check_callback)


rax2 = plt.axes([0.05, 0.6, 0.1, 0.15])
btn = Button(rax2, "next")
i = 0
N = len(s)
def next_callback(event):
	global i
	i = (i+1)%N
	l.set_ydata(s[i])
	plt.draw()

btn.on_clicked(next_callback)
	
	
plt.show()