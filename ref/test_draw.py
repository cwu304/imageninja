import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

x,y,temp = np.loadtxt('data.txt').T #Transposed for easier unpacking
nrows, ncols = 100, 100
grid = temp.reshape((nrows, ncols))

plt.imshow(grid, extent=(x.min(), x.max(), y.max(), y.min()),
           interpolation='nearest', cmap=cm.gist_rainbow)
plt.show()
