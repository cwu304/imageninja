import numpy as np
import matplotlib.pyplot as plt

file_list = ['a', 'b', 'c', 'd']

ax = plt.subplot(111)

### vvv --- first attempt using 'plt.waitforbuttonpress' -----------------------
##plt.hold(False)
##for index, file in enumerate(file_list):
##    # im = plt.imread(file)
##    im = np.random.uniform(size=(30, 20)) # some random data
##    ax.imshow(im)
##    ax.set_title("use (key-press) or reject (button press) image %i" % (index))
##    plt.draw()
##    result = plt.waitforbuttonpress()
##    print "result :", result

##plt.show()
### ^^^ --- first attempt using 'plt.waitforbuttonpress' -----------------------


# vvv --- second attempt using 'plt.connect and key press events' ------------

def event_fct(event):
    """ evaluate key-press events """
    global counter
    if event is None:
        counter = 0
    else:
        if event.key in ['r', 'u']:
            if event.key in 'r':
                print "reject image"
            elif event.key in 'u':
                print "use image"

            # go to next image
            counter += 1
        else:
            print "Key >%s< is not defined" % (event.key)
            return

    # im = plt.imread(file_list[counter])
    im = np.random.uniform(size=(30, 20)) # some random data
    ax.imshow(im)
    ax.set_title("[u]se or [r]eject image %i by key press" % (counter))
    plt.draw()

event_fct(None) # initial setup
plt.connect('key_press_event', event_fct)
plt.show()
# ^^^ --- second attempt using 'plt.connect and key press events' ------------
