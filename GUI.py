from __future__ import division

#import Image
import Tkinter
import tkFileDialog
from math6643 import SVD_Solver
from test6643 import test6643
from train6643 import train6643
from scipy import misc
import matplotlib.pyplot as plot
import numpy

'''

GUI constitution:

1=>titlebar

2=>hintbar

3=>a.openFileButton b.draw_origin_button c. draw_origin_radio

4=>a.SVD_button b.how_SVD_radio

5=>a.SVD_compress_button b. SVD_rank_select_hint c. SVD_rank_entry d. SVD_show_image_button e. draw_SVD_radio

5=>TODO:face recognize button

6=> 
'''
class GUI:

    def __init__(self,rootWin):
        self.figure_num=1
        self.win=rootWin
        self.win.title('Image Ninja!')
        self.filename=Tkinter.StringVar()
        self.image=None #the original image
        #self.train=[]
        self.imageMax=0#to solve the spots
        self.imageMin=0
        self.CONST_SVD_BUILTIN=1
        self.CONST_SVD_1=2
        self.CONST_R=0
        self.CONST_G=1
        self.CONST_B=2
        #A=USV(T)
        self.SVD_solver_builtin=None
        self.SVD_solver_1=None
        self.SVD_color_solver_builtin=None
        self.SVD_color_solver_1=None
        self.CONST_GRAY=1
        self.CONST_COLOR=2

        titlebar=Tkinter.Label(self.win,text='Welcome to Image Ninja')
        titlebar.grid(row=1,column=4)
        
        self.hint=Tkinter.StringVar()
        hintbar=Tkinter.Label(self.win,textvariable=self.hint)
        hintbar.grid(row=2,column=2,columnspan=6)

        self.gray_or_color=Tkinter.IntVar()
        gray_or_color_radio_1=Tkinter.Radiobutton(self.win, text='Gray Scale', variable=self.gray_or_color, value=self.CONST_GRAY)
        gray_or_color_radio_2=Tkinter.Radiobutton(self.win, text='Color', variable=self.gray_or_color, value=self.CONST_COLOR)
        gray_or_color_radio_1.grid(row=3, column=1)
        gray_or_color_radio_2.grid(row=3,column=3)
        
        openFileButton=Tkinter.Button(self.win,text="Select File", command=self.openFileClicked)
        openFileButton.grid(row=5,column=1)

        draw_origin_button=Tkinter.Button(self.win,text="Draw Original Image", command=self.drawOrigin)
        draw_origin_button.grid(row=5,column=2)
        
        SVD_button=Tkinter.Button(self.win,text="Do SVD", command=self.doSVD)
        SVD_button.grid(row=6, column = 1)

        self.how_SVD = Tkinter.IntVar()
        how_SVD_radio_1=Tkinter.Radiobutton(self.win, text='By Scipy built-in', variable=self.how_SVD, value=self.CONST_SVD_BUILTIN)
        how_SVD_radio_2=Tkinter.Radiobutton(self.win, text='Try our implementation', variable=self.how_SVD, value=self.CONST_SVD_1)
        how_SVD_radio_1.grid(row=6, column=2)
        how_SVD_radio_2.grid(row=6, column=3)
        
        SVD_rank_select_hint = Tkinter.Label(self.win,text="RANK:")
        SVD_rank_select_hint.grid(row=7,column=1)
        
        self.SVD_rank_entry=Tkinter.Entry(self.win)
        self.SVD_rank_entry.grid(row=7,column=2)

        SVD_show_image_button = Tkinter.Button(self.win, text="Compress/Filter", command=self.draw_SVD)
        SVD_show_image_button.grid(row=7, column=3)

        train_button=Tkinter.Button(self.win, text="Train from training/", command=self.train)
        test_button=Tkinter.Button(self.win, text="Test with this image",command=self.test)
        train_button.grid(row=8, column=2)
        test_button.grid(row=8, column=3)

        self.maxWindow()


        
    def openFileClicked(self):
        self.win.withdraw()
        self.filename=tkFileDialog.askopenfilename()
        try:
            self.image=misc.imread(self.filename)#this is a ndarray of numpy
        except:
            self.hint.set('invalid file')
        self.win.deiconify()
        

    def drawOrigin(self):
        self.hint.set('Draw the original image of ' + str(self.filename) )      
        fig=plot.figure(self.figure_num)
        fig.canvas.set_window_title('original image of ' + self.filename)
        self.figure_num+=1
        plot.imshow(self.image,cmap=plot.cm.gray)
        plot.title('Original Image from ' + self.filename)
        plot.show()
       

    def maxWindow(self):
        toplevel=self.win.winfo_toplevel()
        try:
            # On MS Windows one can set the "zoomed" state.
            toplevel.wm_state('zoomed')
        except:
            w = self.win.winfo_screenwidth()
            h = self.win.winfo_screenheight() - 60
            geom_string = "%dx%d+0+0" % (w,h)
            self.win.wm_geometry(geom_string)
        
    def doSVD(self):
        isColor=self.gray_or_color.get()
        if isColor == 0:
            self.hint.set('Please tell me this is grayscale or colored image')
            return
        
        how=self.how_SVD.get()
        if self.image is None:
            self.hint.set('load an image file first')
            return
        elif how == 0:
            self.hint.set('how to do SVD?')
            return
        
        elif how == self.CONST_SVD_BUILTIN:
            self.hint.set('SVD by Builtin Function')
            if isColor==self.CONST_GRAY:
                self.SVD_solver_builtin=SVD_Solver(self.image)              
                self.SVD_solver_builtin.run_builtin()
            elif isColor==self.CONST_COLOR:
                self.SVD_color_solver_builtin=[]
                for rgb in range(3):
                    matrix=numpy.zeros((self.image.shape[0],self.image.shape[1]),dtype=numpy.uint8)
                    for i in range(self.image.shape[0]):
                        for j in range(self.image.shape[1]):
                            matrix[i][j]=self.image[i][j][rgb]
                    self.SVD_color_solver_builtin.append(SVD_Solver(matrix))
                    #print matrix
                
                for rgb in range(3):
                    self.SVD_color_solver_builtin[rgb].run_builtin()

        elif how==self.CONST_SVD_1:
            if isColor==self.CONST_GRAY: # Chao 4/13
                self.SVD_solver_1=SVD_Solver(self.image)
                self.hint.set('SVD by Our Method 1')
                self.SVD_solver_1.run_implementation_1()
            elif isColor==self.CONST_COLOR:
				# Chao 4/13
                #pass
                self.SVD_color_solver_1=[]
                for rgb in range(3):
                    matrix=numpy.zeros((self.image.shape[0],self.image.shape[1]),dtype=numpy.uint8)
                    for i in range(self.image.shape[0]):
                        for j in range(self.image.shape[1]):
                            matrix[i][j]=self.image[i][j][rgb]
                    self.SVD_color_solver_1.append(SVD_Solver(matrix))
                    #print matrix
                
                for rgb in range(3):
                    self.SVD_color_solver_1[rgb].run_implementation_1()
				#Chao end
    def draw_SVD(self):
        how=self.how_SVD.get()
        rank=self.SVD_rank_entry.get()
        try:
            rank=int(rank)
        except:
            self.hint.set('examine your rank')
            return

        if(rank<=0):
            self.hint.set('bad rank')

        #examining the settings
        isColor=self.gray_or_color.get()
        solver=None
        if how==0:
            self.hint.set('do SVD first')
            return
        elif how==self.CONST_SVD_BUILTIN:          
            self.hint.set('draw SVD @ ' + str(rank))
            if isColor==self.CONST_GRAY:
                solver=self.SVD_solver_builtin                
            elif isColor==self.CONST_COLOR:
                solver=self.SVD_color_solver_builtin
                
        elif how==self.CONST_SVD_1:
	        #Chao
            #add the color and grayscale teller
            self.hint.set('draw SVD @ ' + str(rank))
            if isColor==self.CONST_GRAY:
                solver=self.SVD_solver_1                
            elif isColor==self.CONST_COLOR:
                solver=self.SVD_color_solver_1
            #solver=self.SVD_solver_1
            #Chao end
        #everything is good
        #restore the image for i=1~rank
        approx=None
        if isColor==self.CONST_GRAY:
            approx=solver.find_approximate(rank).astype(numpy.uint8)
            #print approx
        elif isColor==self.CONST_COLOR:
            approx=numpy.zeros(self.image.shape,dtype=numpy.uint8) #Chao
            for rgb in range(3):
                '''
                max_pixel=self.image[0][0][rgb]
                min_pixel=max_pixel
                for i in range(self.image.shape[0]):
                    for j in range(self.image.shape[1]):
                        if self.image[i][j][rgb] > max_pixel:
                            max_pixel=self.image[i][j][rgb]
                        if self.image[i][j][rgb] < min_pixel:
                            min_pixel=self.image[i][j][rgb]
                print 'max/min pixel is {0} and {1}'.format(max_pixel,min_pixel)
                '''
                mat=solver[rgb].find_approximate(rank).astype(numpy.uint8)
                for i in range(self.image.shape[0]):
                    for j in range(self.image.shape[1]):
                        
                        if mat[i][j] > 255:
                            approx[i][j][rgb]=255
                        elif mat[i][j] < 0:
                            approx[i][j][rgb]=0
                        else:
                            approx[i][j][rgb]=mat[i][j]
                        
                        
            #print approx
            
        fig=plot.figure(self.figure_num)
        percentage=rank*(self.image.shape[0]+self.image.shape[1])
        print 'before compress {}'.format(self.image.shape[0]*self.image.shape[1])
        print 'after compress {}'.format((self.image.shape[0]+self.image.shape[1])*rank)
        percentage=percentage/(self.image.shape[0]*self.image.shape[1])*100
        plot.title('The storage is ' + str(percentage) + '% of the original in size')
        fig.canvas.set_window_title('SVD approximate @ rank = ' + str(rank) +' image of ' + self.filename)
        self.figure_num+=1
        plot.imshow(approx,cmap=plot.cm.gray)
        plot.show()

    def train(self):
        self.hint.set('This might take a bit long...')
        self.train=train6643(3)#train is return by linalg.svd(A)
        self.hint.set('training completed')
        
    def test(self):
        if self.image is None:
            self.hint.set('choose an image first')
            return

        test=test6643(self.train, self.image,3)
        fig=plot.figure(self.figure_num)
        plot.title('I think you mean ...')
        fig.canvas.set_window_title('I think you mean ...')
        self.figure_num+=1
        parent='training/'+test[0]
        origin=misc.imread(parent)
        plot.imshow(origin,cmap=plot.cm.gray)
        plot.show()
        

app=GUI(Tkinter.Tk())
app.win.mainloop()
