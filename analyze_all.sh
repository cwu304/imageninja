OUTPUT=results/analysis.txt
files=`ls results/result*.txt`
#echo $files

echo > $OUTPUT
for file in $files
do
    python analyze_result.py $file >> $OUTPUT
done